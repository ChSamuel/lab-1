from django.shortcuts import render
from datetime import datetime, date

mhs_name = 'Christopher Samuel'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 1, 21)
npm = 1806141151
location = 'Fakultas Ilmu Komputer UI'
angkatan = 2018
hobby = "Mendengarkan musik"
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'angkatan': angkatan,
                'location': location, 'hobby': hobby}
    return render(request, 'WebsiteChristopherSamuel.html', response)

def calculate_age(birth_year):
    if birth_year <= curr_year :
        return curr_year - birth_year
    else :
        return 0
